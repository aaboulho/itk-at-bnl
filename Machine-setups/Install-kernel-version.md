# Kernel installation 

The purpose of these instructions is to explain how to install a new kernel version, for example on an HP machine with CENTOS 7. 

## Example commands 

Mainly throwing commands here so I don't lose them or have to waste time re-finding them.

```
yum --enablerepo="elrepo-kernel" list available | grep "kernel-" # list available kernel versions
install rpm file: sudo rpm -i <file.rpm>
install rpm file example: sudo rpm -i Downloads/kernel-ml-5.15.11-1.el7.elrepo.x86_64.rpm
```

- On an HP machine, hold F10 to enter BIOS. Can disable secure boot there in case you want to use an elrepo kernel and also are not able to add it as a secure boot option. 

## Useful links 

- Many elrepo versions: https://linux.cc.iitk.ac.in/mirror/centos/elrepo/kernel/el7/x86_64/RPMS/
- https://www.fosslinux.com/7487/how-to-install-the-latest-linux-kernel-in-centos-7.htm
- https://elrepo.org/tiki/SecureBootKey # didn't work for me for some reason (even with sudo)
- https://computingforgeeks.com/install-linux-kernel-5-on-centos-7/
