# No space left on device 

An issue that has come up several times is what appears to be no space left on a device, which may be related to infux_db uploading, and therefore breaks the chain towards grafana plotting. An example output of this issue, which happens upon starting the ColdJig GUI, is the following:

```
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 233, in core_loop
    PID(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 166, in PID
    logger.debug("PID Update")
Message: 'PID Update'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 255, in core_loop
    upload_to_db(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 174, in upload_to_db
    logger.debug("Upload latest data to INFLUX")
Message: 'Upload latest data to INFLUX'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 255, in core_loop
    upload_to_db(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 182, in upload_to_db
    Influx.WriteToDatabase(data_dict,status)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/ITSDAQ_COMM/ITK_INFLUX.py", line 110, in WriteToDatabase
    logger.debug(f"status update: {status}")
Message: 'status update: IDLE'
Arguments: ()
(500)
Reason: Internal Server Error
HTTP response headers: HTTPHeaderDict({'Content-Type': 'application/json', 'Request-Id': '32a75612-6f61-11ed-9b8b-dca6321b5552', 'X-Influxdb-Build': 'OSS', 'X-Influxdb-Error': 'write /var/lib/influxdb/meta/meta.dbtmp: no space left on device', 'X-Influxdb-Version': '1.8.3', 'X-Request-Id': '32a75612-6f61-11ed-9b8b-dca6321b5552', 'Date': 'Mon, 28 Nov 2022 21:11:17 GMT', 'Content-Length': '77'})
HTTP response body: {"error":"write /var/lib/influxdb/meta/meta.dbtmp: no space left on device"}


[16:11:17]: WARNING - coldjiglib - slow down rate - each loop iteration takes 6.792491538042668 seconds
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 265, in core_loop
    logger.warning(f"slow down rate - each loop iteration takes {dt} seconds")
Message: 'slow down rate - each loop iteration takes 6.792491538042668 seconds'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 232, in core_loop
    read_hw(hw_read_list,data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 126, in read_hw
    hw.Read(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/Hardware/DewPoint/DewPoint.py", line 43, in Read
    data_dict[f'DP.{self.id}'] = self.DP_calc(air_temp,rh)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/Hardware/DewPoint/DewPoint.py", line 69, in DP_calc
    logger.debug(f'Air Temperature: {air_temp} C / {air_temp_kelvin} k')
Message: 'Air Temperature: 22.434051513671875 C / 295.58405151367185 k'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 232, in core_loop
    read_hw(hw_read_list,data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 126, in read_hw
    hw.Read(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/Hardware/DewPoint/DewPoint.py", line 43, in Read
    data_dict[f'DP.{self.id}'] = self.DP_calc(air_temp,rh)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/Hardware/DewPoint/DewPoint.py", line 72, in DP_calc
    logger.debug(f'Dew Point={dp} C')
Message: 'Dew Point=-99.9 C'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 232, in core_loop
    read_hw(hw_read_list,data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 126, in read_hw
    hw.Read(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/Hardware/DewPoint/DewPoint.py", line 43, in Read
    data_dict[f'DP.{self.id}'] = self.DP_calc(air_temp,rh)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/Hardware/DewPoint/DewPoint.py", line 69, in DP_calc
    logger.debug(f'Air Temperature: {air_temp} C / {air_temp_kelvin} k')
Message: 'Air Temperature: 22.372382202148444 C / 295.52238220214844 k'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 232, in core_loop
    read_hw(hw_read_list,data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 126, in read_hw
    hw.Read(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/Hardware/DewPoint/DewPoint.py", line 43, in Read
    data_dict[f'DP.{self.id}'] = self.DP_calc(air_temp,rh)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/Hardware/DewPoint/DewPoint.py", line 72, in DP_calc
    logger.debug(f'Dew Point={dp} C')
Message: 'Dew Point=-99.9 C'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 233, in core_loop
    PID(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 166, in PID
    logger.debug("PID Update")
Message: 'PID Update'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 255, in core_loop
    upload_to_db(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 174, in upload_to_db
    logger.debug("Upload latest data to INFLUX")
Message: 'Upload latest data to INFLUX'
Arguments: ()
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 255, in core_loop
    upload_to_db(data_dict)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 182, in upload_to_db
    Influx.WriteToDatabase(data_dict,status)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/ITSDAQ_COMM/ITK_INFLUX.py", line 110, in WriteToDatabase
    logger.debug(f"status update: {status}")
Message: 'status update: IDLE'
Arguments: ()
(500)
Reason: Internal Server Error
HTTP response headers: HTTPHeaderDict({'Content-Type': 'application/json', 'Request-Id': '36b50a0e-6f61-11ed-9ba3-dca6321b5552', 'X-Influxdb-Build': 'OSS', 'X-Influxdb-Error': 'write /var/lib/influxdb/meta/meta.dbtmp: no space left on device', 'X-Influxdb-Version': '1.8.3', 'X-Request-Id': '36b50a0e-6f61-11ed-9ba3-dca6321b5552', 'Date': 'Mon, 28 Nov 2022 21:11:23 GMT', 'Content-Length': '77'})
HTTP response body: {"error":"write /var/lib/influxdb/meta/meta.dbtmp: no space left on device"}


[16:11:23]: WARNING - coldjiglib - slow down rate - each loop iteration takes 6.678351979993749 seconds
--- Logging error ---
Traceback (most recent call last):
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1085, in emit
    self.flush()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/logging/__init__.py", line 1065, in flush
    self.stream.flush()
OSError: [Errno 28] No space left on device
Call stack:
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 890, in _bootstrap
    self._bootstrap_inner()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 932, in _bootstrap_inner
    self.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/threading.py", line 870, in run
    self._target(*self._args, **self._kwargs)
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 80, in _worker
    work_item.run()
  File "/home/pi/.pyenv/versions/3.8.0/lib/python3.8/concurrent/futures/thread.py", line 57, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/home/pi/Desktop/Coldjig_SW/coldjiglib2/coldjiglib.py", line 265, in core_loop
    logger.warning(f"slow down rate - each loop iteration takes {dt} seconds")
Message: 'slow down rate - each loop iteration takes 6.678351979993749 seconds'
Arguments: ()
```

Specifically, it appears the no space issues comes up upon a call of `flush` from python's `logging` method. 


```
sudo du -sh ./* | sort -h 
```

Results in:

```
(coldbox_controller_webgui) pi@raspberrypi:/ $ sudo du -sh ./* | sort -h 
du: cannot read directory './proc/8909/task/8909/net': Invalid argument
du: cannot read directory './proc/8909/net': Invalid argument
du: cannot access './proc/9425/task/9425/fd/3': No such file or directory
du: cannot access './proc/9425/task/9425/fdinfo/3': No such file or directory
du: cannot access './proc/9425/fd/3': No such file or directory
du: cannot access './proc/9425/fdinfo/3': No such file or directory
du: cannot access './run/user/1000/gvfs': Permission denied
0       ./dev
0       ./proc
0       ./sys
4.0K    ./mnt
4.0K    ./srv
8.0K    ./media
16K     ./lost+found
64K     ./tmp
7.1M    ./etc
9.3M    ./bin
11M     ./sbin
32M     ./root
45M     ./opt
52M     ./boot
188M    ./run
345M    ./lib
3.1G    ./var
3.3G    ./home
5.1G    ./usr
```

Indicating the directories with the most space used. 

This command:

```
(coldbox_controller_webgui) pi@raspberrypi:~ $ df
Filesystem     1K-blocks     Used Available Use% Mounted on
/dev/root       12420920 12404536         0 100% /
devtmpfs         1867792        0   1867792   0% /dev
tmpfs            1999888        0   1999888   0% /dev/shm
tmpfs            1999888   183788   1816100  10% /run
tmpfs               5120        4      5116   1% /run/lock
tmpfs            1999888        0   1999888   0% /sys/fs/cgroup
/dev/mmcblk0p6    258094    53036    205058  21% /boot
tmpfs             399976        0    399976   0% /run/user/1000
```

indicates that the root filesystem is in full use (mounted on `/`). 

https://www.amirootyet.com/post/varlog-disk-space-issues-ubuntu-kali/

(At this point I am just taking notes which will be cleaned later)

`sudo find / -size +1000M` # find any files with size > 1GB on machine. 
`ls -s -S /var/log` # list files in order of size 

Log rotating: `https://www.tecmint.com/install-logrotate-to-manage-log-rotation-in-linux/`

Do we need to add grafana/influx to logrotation? 

## Temporary, unideal solution

`https://askubuntu.com/questions/515146/very-large-log-files-what-should-i-do`

```
cd /var/log
sudo su 
> syslog 
> daemon.log
exit
sudo reboot 
```

Note that after rebooting, you may need to reconfigure the IP of the data source for grafana. Check that the correct IP for the influx DB data source is set by going to, for example currently for BNL coldbox 2: `http://10.2.231.5:3000` and after signing in with `admin`, (ask expert for password, Abe should know), lefthand bar: `configuration` --> `Data sources` --> should have a source with a name similar to `InfluxDB`, but also should explicitly check data source being used by your grafana dashboard. Then set the URL to the proper IP being used by your influxDB/grafana/gui setup.  

Can even see influxd is filling one of these by default:

```
pi@raspberrypi:/var/log $ tail syslog
Nov 28 17:11:21 raspberrypi influxd[508]: ts=2022-11-28T22:11:21.677486Z lvl=info msg="Compacting file" log_id=0eKGgI0l000 engine=tsm1 tsm1_strategy=full tsm1_optimize=false trace_id=0eS4ZPo0001 op_name=tsm1_compact_group tsm1_index=0 tsm1_file=/var/lib/influxdb/data/_internal/monitor/728/000000001-000000001.tsm
Nov 28 17:11:21 raspberrypi influxd[508]: ts=2022-11-28T22:11:21.677279Z lvl=info msg="Cache snapshot (end)" log_id=0eKGgI0l000 engine=tsm1 trace_id=0eS4ZPo0000 op_name=tsm1_cache_snapshot op_event=end op_elapsed=1.233ms
Nov 28 17:11:21 raspberrypi influxd[508]: ts=2022-11-28T22:11:21.677537Z lvl=info msg="Compacting file" log_id=0eKGgI0l000 engine=tsm1 tsm1_strategy=full tsm1_optimize=false trace_id=0eS4ZPo0001 op_name=tsm1_compact_group tsm1_index=1 tsm1_file=/var/lib/influxdb/data/_internal/monitor/728/000000002-000000001.tsm
Nov 28 17:11:21 raspberrypi influxd[508]: ts=2022-11-28T22:11:21.677590Z lvl=info msg="Compacting file" log_id=0eKGgI0l000 engine=tsm1 tsm1_strategy=full tsm1_optimize=false trace_id=0eS4ZPo0001 op_name=tsm1_compact_group tsm1_index=2 tsm1_file=/var/lib/influxdb/data/_internal/monitor/728/000000003-000000001.tsm
Nov 28 17:11:21 raspberrypi influxd[508]: ts=2022-11-28T22:11:21.677589Z lvl=info msg="Error writing snapshot" log_id=0eKGgI0l000 engine=tsm1 error="error opening new segment file for wal (1): write /var/lib/influxdb/wal/_internal/monitor/728/_00019.wal: no space left on device"
Nov 28 17:11:21 raspberrypi influxd[508]: ts=2022-11-28T22:11:21.677640Z lvl=info msg="Compacting file" log_id=0eKGgI0l000 engine=tsm1 tsm1_strategy=full tsm1_optimize=false trace_id=0eS4ZPo0001 op_name=tsm1_compact_group tsm1_index=3 tsm1_file=/var/lib/influxdb/data/_internal/monitor/728/000000004-000000001.tsm
Nov 28 17:11:22 raspberrypi influxd[508]: ts=2022-11-28T22:11:22.676068Z lvl=info msg="Cache snapshot (start)" log_id=0eKGgI0l000 engine=tsm1 trace_id=0eS4ZTi0000 op_name=tsm1_cache_snapshot op_event=start
Nov 28 17:11:22 raspberrypi influxd[508]: ts=2022-11-28T22:11:22.676173Z lvl=info msg="Cache snapshot (end)" log_id=0eKGgI0l000 engine=tsm1 trace_id=0eS4ZTi0000 op_name=tsm1_cache_snapshot op_event=end op_elapsed=0.149ms
Nov 28 17:11:22 raspberrypi influxd[508]: ts=2022-11-28T22:11:22.676281Z lvl=info msg="Error writing snapshot" log_id=0eKGgI0l000 engine=tsm1 error="error opening new segment file for wal (1): write /var/lib/influxdb/wal/_internal/monitor/728/_00019.wal: no space left on device"
Nov 28 17:11:22 raspberrypi influxd[508]: ts=2022-11-28T22:11:22.742155Z lvl=warn msg="Error compacting TSM files" log_id=0eKGgI0l000 engine=tsm1 tsm1_strategy=full tsm1_optimize=false trace_id=0eS4ZPo0001 op_name=tsm1_compact_group error="write /var/lib/influxdb/data/_internal/monitor/728/000000004-000000002.tsm.tmp: no space left on device"
pi@raspberrypi:/var/log $ 
```

Thing to look into: influxd (or influxdb) log rotating.
