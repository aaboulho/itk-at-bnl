# Known issues 

The purpose of this file / directory is to keep track of known issues and log their solutions, in case the issue comes up again, so that the user can solve it. Note that in general if some of this information is already kept track of somewhere in a different documentation area, the know issue description and its solution can simply be merged to the desired common documentation area.

