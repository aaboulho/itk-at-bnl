# InfluxDB-Grafana

The purpose of this repository is to describe how to setup [InfluxDB](https://en.wikipedia.org/wiki/InfluxDB) uploads and plotting with [Grafana](https://en.wikipedia.org/wiki/Grafana) from scratch, and to save useful configurations etc. 

Initial help and most of the commands below came from this useful webpage: [Setup influxDB + Grafana startup on Raspberry Pi](https://simonhearne.com/2020/pi-influx-grafana/). To do this from scratch on my machine I performed the following:

## InfluxDB

To install InfluxDB:

```
wget -qO- https://repos.influxdata.com/influxdb.key | sudo apt-key add - \
source /etc/os-release \
echo "deb https://repos.influxdata.com/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
```

Then to run, in one terminal I ran the command `influxd`, and in another terminal I ran `influx`. 

After creating a username and password, you can start uploading random numbers to InfluxDB with:

```
wget https://gitlab.cern.ch/atishelm/influxdb-grafana/-/raw/9b891e4d6c52d76270589e66ae1a2d71c969e695/Random_Number_Uploader.py
<update username, password in above file>
chmod +x Random_Number_Uploader.py # Make executable
watch -n 3 ./Random_Number_Uploader.py # Upload a random number every 3 seconds to InfluxDB
```

You should see some sort of messages in your terminal running influxd, similar to:

```
[httpd] 127.0.0.1 - Abe [13/Oct/2022:14:54:13 -0400] "POST /write?db=home HTTP/1.1 " 204 0 "-" "python-requests/2.25.1" 6dd4970d-4b28-11ed-8690-9cb6d01dff8e 3529
```

Something is now being posted to InfluxDB, but we want to beautifully display it - we can use Grafana for that.

## Grafana 

To install grafana:

```
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add - \
echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee /etc/apt/sources.list.d/grafana.list
```

How I started an instance of grafana:

```
sudo service grafana-server start 
```

Then on a web browser (Chrome) I navigated to: `http://localhost:3000/`, "Add your first data source", InfluxDB. 

## InfluxDB datasource

The connection between InfluxDB and Grafana is made by adding InfluxDB as a data source. One gets there either from the "Add your first data source" panel on the front page, or on the bottom left: "Configuration" --> "Data sources".

```
URL: "http://127.0.0.1:8086"
Database: <nameFromInfluxDBSetup>
HTTP Method: "GET"
```

## Dashboard 

To make a new dashboard: "Dashboards" on left-hand menu, "New" on right-hand side, "Import", "Upload JSON file". Got first JSON file example from here: [Pi stats grafana](https://gist.github.com/simonhearne/e4fd4d9f94c302a5847324a48724c6eb). Made a new version here: [Grafana dashboard example for random number plotting](https://gitlab.cern.ch/atishelm/influxdb-grafana/-/raw/e2e066c051540ac9d8769a63a6ee4d22a03f4985/Random_Number_Dashboard.json). 

If everything worked well, you should now see the random numbers from InfluxDB plotted on the Grafana plot, looking something like below:

![Random_Number_Plot.png](Random_Number_Plot.png "Look at that beautiful, meaningless plot!")

## Useful links 

Export and import dashboards: https://grafana.com/docs/grafana/v9.0/dashboards/export-import/
https://www.thepythoncode.com/article/extract-weather-data-python
