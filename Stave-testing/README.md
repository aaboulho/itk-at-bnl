# Stave testing 

The purpose of this directory is to store documentation for performing stave testing at BNL. 

Version 1 of the software used for stave testing can be found [here](https://gitlab.cern.ch/grosin/radiation). Private forked version which has more updated files w.r.t. main repo can be found [here](https://gitlab.cern.ch/atishelm/radiation/-/tree/AbeForkBranch). 
