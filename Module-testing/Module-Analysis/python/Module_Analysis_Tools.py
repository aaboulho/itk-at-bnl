"""
28 December 2022
Abraham Tishelman-Charny

The purpose of this module is to define parameters for Module-Analysis.py
"""

# parameters 
hist_color = 'C0'

# parameters 
modules = [
    "BNL-PPB2-MLS-111",
    "BNL-PPB2-MLS-112",
    "BNL-PPB2-MLS-113",
    "BNL-PPB2-MLS-114",
]
# quantities = [
#     "gain", "vt50", "innse" # innse = input noise 
# ]

quantities = [
    "Current", "Voltage" 
]

quantity_dict = {
    "gain" : "Gain",
    "vt50" : "vt50", 
    "innse" : "Input_noise",
    "Current" : "Current",
    "Voltage" : "Voltage"
}

RUN_NUMBER = "201"
SCAN_NUMBER = "12"
#RESULT_TYPE = "RC"
RESULT_TYPE = "IandV"
#inDir = "/eos/user/a/atishelm/www/ITk/Thermal-Cycling/ThreePointGain/FullTest/results/"
#ol = "/eos/user/a/atishelm/www/ITk/Thermal-Cycling/ThreePointGain/FullTest/"