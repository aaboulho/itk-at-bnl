"""
28 December 2022
Abraham Tishelman-Charny

The purpose of this python module is to set run options for the Module-Analysis module.
"""

# Imports 
import argparse 

# Create parser and get command line arguments 
parser = argparse.ArgumentParser()
parser.add_argument("--dryRun", action="store_true", help="To run without submission")
parser.add_argument("--plotPerModule", action="store_true", help="Plot quantities for each module")
parser.add_argument("--plotRatios", action="store_true", help="Plot ratio of modules together")
parser.add_argument("--dataFromITSDAQ", action="store_true", help="Input data is of txt format from ITSDAQ output")
parser.add_argument("--dataFromCSV", action="store_true", help="Custom data in CSV format")
parser.add_argument("--inDir", type=str, default="/eos/user/a/atishelm/www/ITk/Thermal-Cycling/HV-Stability/data/", help="Directory with data to input")
parser.add_argument("--ol", type=str, default="/eos/user/a/atishelm/www/ITk/Thermal-Cycling/HV-Stability/", help="Output location for plots.")
options = parser.parse_args()