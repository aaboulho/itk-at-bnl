"""
28 December 2022
Abraham Tishelman-Charny

The purpose of this python module is to plot, compare, and analyze noise from ITk strips modules.

Example usage:
python3 Module-Analysis.py --plotRatios --dataFromITSDAQ
python3 Module-Analysis.py --plotPerModule --dataFromCSV

"""

# imports 
import pandas as pd 
import numpy as np 
import os 
import csv 
from matplotlib import pyplot as plt 
import mplhep as hep 
plt.style.use(hep.style.ATLAS) 
from python.Module_Analysis_Tools import * 
from python.SetOptions import options 

# Command line flag options 
plotPerModule = options.plotPerModule
plotRatios = options.plotRatios
inDir = options.inDir 
ol = options.ol 
dataFromITSDAQ = options.dataFromITSDAQ
dataFromCSV = options.dataFromCSV

if(dataFromITSDAQ + dataFromCSV != 1):
    raise Exception("Need to choose either dataFromITSDAQ or dataFromCSV")

# Create output directory if it doesn't already exist
if(not os.path.exists(ol)):
    print("Creating output directory:",ol)
    os.system("mkdir -p %s"%(ol))
    os.system("cp %s/../index.php %s"%(ol, ol)) # assumes one directory up from 'ol' exists

# Functions 
def Save_Figures(fig, fileTypes_, OUTNAME_, plotType):
    for fileType in fileTypes_:
        outname = "%s_%s.%s"%(OUTNAME_, plotType, fileType)
        print("")
        print("Saving figure:",outname)
        print("")
        fig.savefig(outname)
    plt.close()    

def Make_Plots(x_vals_, x_label, y_vals_, y_vals_err_, OUTNAME_, Module_, Value_type_, SET_NUMBER_, add_side_number):

    unit_dict = {
        "Current" : "nA",
        "Voltage" : "mV"
    }

    yunit = unit_dict[Value_type_]

    # params 
    fileTypes = ["png", "pdf"]
    Nentries = len(y_vals_)
    ymaxIncrement = 0.333
    quantity_nonlfn_label = Value_type_.replace("_", " ")
    y_label = "%s [%s]"%(quantity_nonlfn_label, yunit)

    # series (values over time)
    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
    ax.plot(x_vals_, y_vals_)
    hep.atlas.text("ITk Internal")
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    # set ymax higher to contain plot labels
    ymin, ymax = ax.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    ax.set_ylim(ymin, Incremented_Ymax)

    # Upper right text 
    if(add_side_number): upperText = "%s, side %s"%(Module_,SET_NUMBER_)
    else: upperText = "%s"%(Module_)
    plt.text(
        0.94,
        0.95,
        "\n".join([
            upperText,
            quantity_nonlfn_label
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "series")

    # histogram (values integrated over time)
    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
    counts, bins = np.histogram(y_vals_)
    avg, stdev = np.mean(y_vals_), np.std(y_vals_)
    ax.hist(bins[:-1], bins, weights=counts)
    hep.atlas.text("ITk Internal")    
    ax.set_xlabel(y_label)
    ax.set_ylabel("Entries")

    # set ymax higher to contain plot labels
    ymin, ymax = ax.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    ax.set_ylim(ymin, Incremented_Ymax)

    # Upper left text 
    plt.text(
        0.06,
        0.95,
        "\n".join([
            "$\mu$ = %s" % float('%.4g' % avg),
            "$\sigma$ = %s" % float('%.4g' % stdev),
            "$N_{entries}$ = %s"%(Nentries)
        ]
        ),
        horizontalalignment='left',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    # Upper right text 

    plt.text(
        0.94,
        0.95,
        "\n".join([
            upperText,
            quantity_nonlfn_label
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "history")

    # Save avg and stdev 
    csv_outname = '%s_stats.csv'%(OUTNAME_)
    print("Saving csv file:",csv_outname)
    row = [avg, stdev]
    f = open(csv_outname,'w')
    writer = csv.writer(f)
    writer.writerow(row)
    f.close()

def Make_Ratio_Plot(all_module_vals_, x_vals_, modules_, quantity_, side_, OUTNAME_):
    
    # parameters 
    fileTypes = ["png", "pdf"]
    N_modules = len(all_module_vals_)
    xmin, xmax = min(x_vals_), max(x_vals_)
    ymaxIncrement = 0.5
    quantity_nonlfn_label = quantity_.replace("_", " ")

    # make ratio plot 
    fig, ax = plt.subplots(2, figsize=(6, 4), sharex=True, dpi=100, gridspec_kw={'height_ratios': [3, 1]})

    lower = ax[1]
    upper = ax[0]
    
    hep.atlas.text("ITk Internal", ax=upper) # customization: https://mplhep.readthedocs.io/en/latest/api.html

    for m_i, module_vals in enumerate(all_module_vals_):
        upper.plot(x_vals_, module_vals, 'o-', label = modules_[m_i], linewidth=0.25, markersize=0.25)
        ratio_vals = np.divide(module_vals, all_module_vals_[0])
        lower.plot(x_vals_, ratio_vals, 'o', linewidth=0, markersize=0.25)
    
    lower.set_xlabel("Channel number")
    upper.set_ylabel(quantity_nonlfn_label)    

    upper.set_xlim(xmin, xmax)
    lower.set_xlim(xmin, xmax)
    firstModuleName = modules_[0]
    lowerYlabel = "\n".join([
        "Ratio to         ",
        firstModuleName
    ])
    lower.set_ylabel(lowerYlabel, fontsize=10)

    # set ymax higher to contain plot labels
    ymin, ymax = upper.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    upper.set_ylim(ymin, Incremented_Ymax)    
    upper.legend(fontsize=7.5, loc = 'upper center')

    fig.tight_layout()
    # Stream number 
    plt.text(
        0.94,
        0.95,
        "\n".join([
            "Stream %s"%(int(side_)-1)
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=upper.transAxes
    )    
    Save_Figures(fig, fileTypes, OUTNAME_, "series")    

if(plotPerModule):
    # go through modules 
    for module_i, module in enumerate(modules): # assumes data is in form of txt files output from ITSDAQ
        print("On module:",module)

        if(dataFromITSDAQ):
            inFile = "%s/%s_%s_%s_%s.txt"%(inDir, module, RESULT_TYPE, RUN_NUMBER, SCAN_NUMBER)
            df = pd.read_csv(inFile, delimiter="\t")
            print("df:",df)

            # for each quantity, make a plot for the module
            for quantity in quantities:
                print("On quantity:",quantity)
                vals = np.array(df[quantity].tolist())
                channels = np.array(df["#chan"].tolist())
                strips_1_vals = vals[:1280] # values for first set of strips
                strips_2_vals = vals[1280:] # values for second set of strips 
                strips_1_channels = channels[:1280]
                strips_2_channels = channels[1280:]

                quant_label = quantity_dict[quantity]

                for SET_NUMBER in ["1", "2"]:
                    # make two types of plots: distribution of values, and value vs. channel number 
                    OUTNAME = '%s/%s_%s_%s'%(ol, module, quant_label, SET_NUMBER)
                    exec("These_channels = strips_%s_channels"%(SET_NUMBER))
                    exec("These_vals = strips_%s_vals"%(SET_NUMBER))
                    vals_rms = None
                    x_label = "Channel number"
                    add_side_num = 1
                    Make_Plots(These_channels, x_label, These_vals, vals_rms, OUTNAME, module, quant_label, SET_NUMBER, add_side_num)

        elif(dataFromCSV):
            inFile = "%s/%s_%s.csv"%(inDir, module, RESULT_TYPE)
            df = pd.read_csv(inFile, delimiter=",", header=None)
            print("df",df)
            quant_column_dict = { # columns with quantity and its RMS
                "Current" : [2, 3],
                "Voltage" : [4, 5]
            }
            # for each quantity, make a plot for the module
            for quantity in quantities:
                print("On quantity:",quantity)
                val_col, val_rms_col = quant_column_dict[quantity]

                vals = np.array(df[val_col].tolist())
                vals_rms = np.array(df[val_rms_col].tolist())
                quant_label = quantity_dict[quantity]
                # make two types of plots: distribution of values, and value vs. time (x axis value time now not channel number)
                times = np.array(df[0].tolist())
                initial_time = times[0] 
                times = np.array([time-initial_time for time in times])
                OUTNAME = "%s/%s_%s"%(ol, module, quant_label)
                SET_NUMBER = "SET_NUMBER_BLANK"
                x_label = "time [s]"
                add_side_num = 0
                Make_Plots(times, x_label, vals, vals_rms, OUTNAME, module, quant_label, SET_NUMBER, add_side_num)

if(plotRatios):

    # get all values to then plot together
    for module_i, module in enumerate(modules):
        module_str = module.replace("-","_")
        print("On module:",module)
        inFile = "%s/%s_%s_%s_%s.txt"%(inDir, module, RESULT_TYPE, RUN_NUMBER, SCAN_NUMBER)
        df = pd.read_csv(inFile, delimiter="\t")
        print("df:",df)

        # for each quantity, make a plot for the module
        for quantity in quantities:
            print("On quantity:",quantity)
            vals = np.array(df[quantity].tolist())
            channels = np.array(df["#chan"].tolist())

            strips_1_vals = vals[:1280] # values for first set of strips
            strips_2_vals = vals[1280:] # values for second set of strips 

            # assuming same channels for all
            strips_1_channels = channels[:1280]
            strips_2_channels = channels[1280:]

            exec("%s_%s_1_vals = np.copy(strips_1_vals)"%(module_str, quantity)) # module, quantity, side 
            exec("%s_%s_2_vals = np.copy(strips_2_vals)"%(module_str, quantity)) # module, quantity, side 

    # for each quantity, take the ratio among modules 
    for quantity in quantities:
        print("On quantity:",quantity)
        for side in ["1","2"]:
            print("On side:",side)
            all_module_vals = []
            for module in modules:
                module_str = module.replace("-","_")
                exec("these_vals = %s_%s_%s_vals"%(module_str, quantity, side))
                exec("these_x_vals = strips_%s_channels"%(side)) # taking most recent channel vals. assuming same for all modules
                all_module_vals.append(these_vals)
            quant_label = quantity_dict[quantity]
            OUTNAME = '%s/%s_%s_%s'%(ol, "AllModules", quant_label, side)
            Make_Ratio_Plot(all_module_vals, these_x_vals, modules, quant_label, side, OUTNAME)
