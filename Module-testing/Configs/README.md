# Module configs from DB 

The scripts and python modules in this directory are used to obtain `.det` hybrid configuration files used by ITSDAQ when module testing. The latest version of the ASIC configuration obtention script should be around [here](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts/-/blob/master/strips/modules/getAsicConfig.py).
