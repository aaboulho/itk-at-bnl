"""
1 November 2022
Abraham Tishelman-Charny

The purpose of this python module is to characterize the hardware read/set timing for the BNL ITk strip module testing coldbox.

Example usage: 
python3 CharacterizeTiming.py --dataDirec twoHoursData --PlotTimes
python3 CharacterizeTiming.py --dataDirec twoHoursData --PlotAverages

Some other commands used:

cat data.log | grep Read > ReadVals.log
cat data.log | grep Set > SetVals.log

Some relevant links / documents:
https://github.com/scikit-hep/mplhep/blob/4534ba9df742154b9f5c279e549e260b8e3fe43e/src/mplhep/atlas.py
"""

# Imports 
import os 
import numpy as np 
import pandas as pd 
import numpy as np 
import csv 
from matplotlib import pyplot as plt 
import mplhep as hep 
plt.style.use(hep.style.ATLAS) 
from python.CharacterizeTiming_Tools import * 
from python.SetOptions import options 

# Command line flag options 
dataDirec = options.dataDirec
PlotTimes = options.PlotTimes
PlotAverages = options.PlotAverages
ol = "/eos/user/a/atishelm/www/ITk/Coldjig-Timing/%s/"%(dataDirec)

# Create output directory if it doesn't already exist
if(not os.path.exists(ol)):
    print("Creating output directory:",ol)
    os.system("mkdir -p %s"%(ol))
    os.system("cp %s/../index.php %s"%(ol, ol)) # assumes one directory up from 'ol' exists

# Functions 
def Save_Figures(fig, fileTypes_, OUTNAME_, plotType):
    for fileType in fileTypes_:
        outname = "%s_%s.%s"%(OUTNAME_, plotType, fileType)
        print("")
        print("Saving figure:",outname)
        print("")
        fig.savefig(outname)
    plt.close()    

def Make_Plots(vals_, OUTNAME_, HW_, Value_type_):

    # params 
    fileTypes = ["png", "pdf"]
    Nentries = len(vals_)
    ymaxIncrement = 0.333

    # series (values over time)
    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
    ax.plot(vals_)
    hep.atlas.text("ITk Internal")
    ax.set_xlabel("Loop number")
    ax.set_ylabel("Time (s)")

    # set ymax higher to contain plot labels
    ymin, ymax = ax.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    ax.set_ylim(ymin, Incremented_Ymax)

    # Upper right text 
    plt.text(
        0.94,
        0.95,
        "\n".join([
            "Hardware: %s"%(HW_),
            "%s Times"%(Value_type_)
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "series")

    # histogram (values integrated over time)
    fig, ax = plt.subplots(1, figsize=(6, 4), dpi=100)
    counts, bins = np.histogram(vals_)
    avg, stdev = np.mean(vals_), np.std(vals_)
    ax.hist(bins[:-1], bins, weights=counts)
    hep.atlas.text("ITk Internal")    
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Entries")

    # set ymax higher to contain plot labels
    ymin, ymax = ax.get_ylim()
    Incremented_Ymax = ymax + (ymax-ymin)*ymaxIncrement
    ax.set_ylim(ymin, Incremented_Ymax)

    # Upper left text 
    plt.text(
        0.06,
        0.95,
        "\n".join([
            "$\mu$ = %s" % float('%.4g' % avg),
            "$\sigma$ = %s" % float('%.4g' % stdev),
            "$N_{entries}$ = %s"%(Nentries)
        ]
        ),
        horizontalalignment='left',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    # Upper right text 
    plt.text(
        0.94,
        0.95,
        "\n".join([
            "Hardware: %s"%(HW_),
            "%s Times"%(Value_type_)
        ]
        ),
        horizontalalignment='right',
        verticalalignment='top',
        fontweight = 'bold',
        transform=ax.transAxes
    )

    fig.tight_layout()
    Save_Figures(fig, fileTypes, OUTNAME_, "history")

    # Save avg and stdev 
    csv_outname = '%s_stats.csv'%(OUTNAME_)
    print("Saving csv file:",csv_outname)
    row = [avg, stdev]
    f = open(csv_outname,'w')
    writer = csv.writer(f)
    writer.writerow(row)
    f.close()

# "main" functions (not sure how to make proper python main function work with 'exec' function)
if(PlotTimes):
    print("Plotting time distributions and trends")
    for Value_type in Value_types:
        print("On value type:",Value_type)
        df = pd.read_csv("%s/%sVals.log"%(dataDirec, Value_type), delim_whitespace=True, header = None)

        Ncycles = -1
        # plots for each piece of hardware 
        for HW_i, HW in enumerate(HARDWARE):
            print("On hardware:",HW)
            exec("%s_%s_vals = np.array(df.iloc[%s::14, :][11].tolist())"%(HW, Value_type, HW_i))
            exec("THESE_VALS = np.copy(%s_%s_vals)"%(HW, Value_type))
            exec("OUTNAME = '%s/%s_%s_times'"%(ol, HW, Value_type))
            Ncycles = len(THESE_VALS)
            Make_Plots(THESE_VALS, OUTNAME, HW, Value_type)

        # plots for total 
        print("Ncycles:",Ncycles)
        Total_Times = [] 
        for cycle_i in range(0, Ncycles):
            Total_Time = 0
            for HW in HARDWARE:
                exec("vals = %s_%s_vals"%(HW, Value_type))
                Total_Time += float(vals[cycle_i])
            Total_Times.append(Total_Time)

        Total_Times = np.array(Total_Times)
        OUTNAME = "%s/%s_Total_Times"%(ol,Value_type)
        HW = "All"
        Make_Plots(Total_Times, OUTNAME, HW, Value_type)

        # save for later combination
        exec("Total_Times_%s = np.copy(Total_Times)"%(Value_type))

    # read + set time 
    ReadPlusSetTimes = []
    for i in range(0, len(Total_Times_Read)):
        t = 0
        t += Total_Times_Read[i]
        t += Total_Times_Set[i]
        ReadPlusSetTimes.append(t)

    ReadPlusSetTimes = np.array(ReadPlusSetTimes)
    OUTNAME = "%s/ReadPlusSetTimes"%(ol)
    HW = "All"
    Value_type = "Read$+$Set"
    Make_Plots(ReadPlusSetTimes, OUTNAME, HW, Value_type)

if(PlotAverages):
    print("Plotting average times")

    for Value_type in Value_types:
        print("On value type:",Value_type)

        # plots for each piece of hardware 
        for HW_i, HW in enumerate(HARDWARE):
            #print("On hardware:",HW)
            exec("OUTNAME = '%s/%s_%s_times'"%(ol, HW, Value_type))
            csv_path = "%s_stats.csv"%(OUTNAME)
            if(not os.path.exists(csv_path)):
                print("Cannot find file %s - Skipping."%(csv_path))
                continue 
            # get avg and stdev 
            with open(csv_path, newline='') as csvfile:
                datareader = csv.reader(csvfile, delimiter=',', quotechar='|')
                for row in datareader:
                    avg, stdev = row[0], row[1]
                    # save values to plot next 
                    exec("%s_%s_average = float(avg)"%(HW, Value_type))
                    exec("%s_%s_stdev = float(stdev)"%(HW, Value_type))
        
    for Value_type in Value_types:
        hardwares_i = [] 
        hardwares = [] 
        averages = []
        stdevs = []         
        for HW_i, HW in enumerate(HARDWARE):
            exec("thisAvg = %s_%s_average"%(HW, Value_type))
            exec("thisStdev = %s_%s_stdev"%(HW, Value_type))
            hardwares_i.append(HW_i)
            hardwares.append(HW)
            averages.append(thisAvg)
            stdevs.append(thisStdev)
    
        # plot 
        fig, ax = plt.subplots(1, figsize=(12, 4), dpi=200)
        plt.bar(hardwares_i, averages, color = "C0", width = 0.9)
        plt.scatter(hardwares_i, averages, s=3, color = "C0")
        plt.errorbar(hardwares_i, averages, yerr=stdevs, xerr = None, ls='none', ecolor = 'black')
        plt.xlim(-1, len(hardwares_i)+0.25)
        hep.atlas.text("ITk Internal")
        ax.set_xlabel("Hardware component")
        ax.set_ylabel("Average time (s)")

        labelFontSize = 8

        for HW_i, HW in enumerate(HARDWARE):
            exec("thisAvg = %s_%s_average"%(HW, Value_type))
            exec("thisStdev = %s_%s_stdev"%(HW, Value_type))   
            labelHeight = float(float(thisAvg)+float(thisStdev) + float(0.01))         
            plt.text(
                HW_i,
                labelHeight,
                HW,
                horizontalalignment='center',
                verticalalignment='bottom',
                fontweight = 'bold',
                fontsize = labelFontSize
            )

        # remove negative values due to uncertainty 
        ymin, ymax = ax.get_ylim()
        ax.set_ylim(0, ymax)

        plt.text(
            0.5,
            0.95,
            "Average %s times"%(Value_type),
            horizontalalignment='center',
            verticalalignment='top',
            fontweight = 'bold',
            transform=ax.transAxes,
            fontsize = labelFontSize*3
        )

        plt.xticks([]) 
        fig.tight_layout()
        fileTypes = ["png", "pdf"]
        outName = "%s/Average-%s-times"%(ol, Value_type)
        Save_Figures(fig, fileTypes, outName, "bar")        