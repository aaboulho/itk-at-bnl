// 18 January 2023 
// Abraham Tishelman-Charny
//
// The purpose of this macro is to measure and save AMAC current and voltage values 

#include <iostream>
#include <fstream>
#include <time.h>

void HV_Stability(int N_modules = 4, bool returnAll = 1, unsigned long N_readings = 100, bool printValue = 0, bool doAutoRange_ = 1, int interval = 10, int total_time = 60, int verbose = 1){
	if(verbose) printValue = 1; 

	ofstream dataFile; 
	dataFile.open("dataFile.csv");

	// Measure N times total
	time_t initial_time = time(NULL);
	time_t end_time = initial_time + total_time;
	time_t remaining_time; 

	// values to measure
	float Imean, Irms, Vmean, Vrms, range;
	std::vector<float> TheseVals;
	time_t measurementTime; 
	time_t currentTime = time(NULL);

	while(currentTime < end_time){
		for(int i=0; i < N_modules; i++){
			if(verbose) std::cout << "AMAC number " << i << std::endl; 
			measurementTime = time(NULL);
			TheseVals = AMACStar_readIDET(i, N_readings, printValue, doAutoRange_, returnAll);
			
			Imean = TheseVals[0];
			Irms = TheseVals[1];
			Vmean = TheseVals[2];
			Vrms = TheseVals[3];
			range = TheseVals[4];
		
			dataFile << measurementTime << "," << "AMAC" << i << "," << Imean << "," << Irms << "," << Vmean << "," << Vrms << "," << range << "\n"; 
		}
		if(verbose) std::cout << "Sleeping for " << interval << " seconds..." << std::endl;
		
		sleep(interval);
		currentTime = time(NULL);
		remaining_time = end_time - currentTime;
		
		if(verbose){
			std::cout << "current time: " << currentTime << std::endl;
			std::cout << "end time: " << end_time << std::endl;
			//std::cout << "remaining time: " << remaining_time << " seconds" << std::endl; 
		}
		std::cout << "remaining time: " << remaining_time << " seconds" << std::endl;
	}

	dataFile.close();

}
