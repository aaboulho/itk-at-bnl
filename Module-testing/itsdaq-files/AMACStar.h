#ifndef AMACSTAR_H_INCLUDE
#define AMACSTAR_H_INCLUDE

#include "AMAC_common.h"

enum AMACStarChannel {
  AMACSTAR_AM_VDCDC = 0,
  AMACSTAR_AM_DCDCIN,
  AMACSTAR_AM_NTCX,
  AMACSTAR_AM_NTCY,
  AMACSTAR_AM_NTCPB = 4,
  AMACSTAR_AM_CTAT,
  AMACSTAR_AM_CUR1V,
  AMACSTAR_AM_HVRET,
  AMACSTAR_AM_PTAT = 8,
  AMACSTAR_AM_HREFX,
  AMACSTAR_AM_HREFY,
  AMACSTAR_AM_CAL,
  AMACSTAR_AM_A_VDDLR = 12,
  AMACSTAR_AM_A_CUR10V,
  AMACSTAR_AM_A_VDDHI,
  AMACSTAR_AM_OUTPUT,
};

/***
    Initialise the AMAC configurations to default.

    @param addresses: Communication addresses to use 
    @param channels: Control segment to use, if empty assumes sequential channels.
*/
void AMACStar_initialiseAMACs(std::vector < int > addresses, std::vector < int > channels = {});
/// Write configuration to all AMACs (including setID)
void AMACStar_configureAMACs(int enableDCDC);
/// Set communication IDs on AMAC
void AMACStar_setID(int amac);
/// Write current configuration to AMAC
void AMACStar_writeConfiguration(int amac, int enableDCDC);
/// Write specific register on AMAC
void AMACStar_writeReg(int amac, unsigned long int reg, unsigned long int content);
/**
 * Read one register from one of more AMACs.
 *
 * If amac == -1, read all AMACs, otherwise only one AMAC.
 * Return value is array of values for the AMACs.
 */
std::vector < unsigned long int > AMACStar_readReg(int amac, unsigned long int regaddr, bool printReg=1);
/// Calibrate offsets and NTC ref
void AMACStar_internalCalibrations();
/// Calibrate zero offsets for all channels
void AMACStar_calibrateOffsets(int amac, bool updateOffset=1);
/// Calibrate zero offset
void AMACStar_zeroOffset(int amac, int AMchan, bool updateOffset=1);
/// Calibrate NTC against reference
void AMACStar_NTCref(int amac, int NTC, bool updateVref);
/// Read raw AM value from one of more AMACs
std::vector < int > AMACStar_readAM(int amac, int AMchan, int MUX);
/// Read AM value using configured calibration
std::vector < float > AMACStar_readAM_calibrated(int amac, int AMchan, int MUX);
/**
   Read NTC value.

   @param amac: which AMAC to read (-1 for all)
   @param NTC: select NTC input 0 = HX, 1 = HY, 2 = PB
   @param doAutoRange: select range to put reading between 100 and 1020
*/
std::vector < float > AMACStar_readNTC(int amac, int NTC, bool doAutoRange=1);
/**
   Read temperature via PTAT.

   @param amac: which AMAC to read (-1 for all)
*/
std::vector < float > AMACStar_readPTAT(int amac);

/**
   Read detector currents.

   @param amac: which AMAC to read (-1 for all)
   @param nReadings: how many times to read
   @param printValue: if true print the read value
   @param doAutoRange: if true find range to put value between 200 and 800
*/
std::vector < float > AMACStar_readIDET(int amac, unsigned long int nReadings=1000, bool printValue = 0, bool doAutoRange=1, bool returnAll=0);

/**
   Read NTCpb and PTAT and calculate value at 0C.

   NB both HV and DCDC should be off before running. If DCDC is on, we abort.

   PTAT0 = PTAT -4.85*NTCpb (PTAT0 is the PTAT output at 0C).

   @param amac: which AMAC to read (-1 for all)
   @param nReadings: how many times to average over
   @param updateRef: if true then use this for next PTAT temp reading
*/
std::vector < float > AMACStar_readPTATzero(int amac, int nReadings, bool updateRef = true);

/**
   Turn on off DCDC.

   @param amac: which AMAC to control (-1 for all)
   @param state: 0 - turn off, 1 - turn on
*/
void AMACStar_DCDC(int amac, int state);

/// Report ADCs to stream
void AMACStar_reportADCs(int amac_id, std::ostream &os);
/// Report ADCs to std::cout
void AMACStar_reportADCs(int amac_id);
/// Report all ADCs to file
void AMACStar_reportADCs(std::string fname);

// Random number BERT run (only works on single chips right now)
void BERT_rand_Star(int amacid, uint64_t nEvents = 1000, bool endAtFail = 0, bool slow = 0, int nAttempts = 5, unsigned long long int pattern = 0);

/**
   Run ADC calibration scan on all AMACs.

   Scan external calibration input voltage and read the ADCs.

   @param updateCal: Change calibrated slopes/intercepts based on scan
   @param set_cal: Interface to perform update of external voltage
 */
void AMACStar_calScan(bool updateCal, std::unique_ptr<AMACv2_Calibration> set_cal);

/// Show AMAC calibration in a ROOT canvas
void AMACStar_plotcal();

/// Remove a given AMAC from the list
void AMACStar_removeAMAC(int i);

/// Configure AMAC in a funky way which may be useful as part of a controlled power up sequence
void AMACStar_startupStep(int amac, int step);

/// Read LinPol 1.4V line current
std::vector<float> AMACStar_readLinPolCurr(int amacid);

// These are declared for access from calibration scan
extern std::vector<float> AM_slopes_final;
extern std::vector<float> AM_intercepts_final;

extern std::vector<int> addresses_final;
extern std::vector<TGraph *> AMACgraphs;

#endif // AMACSTAR_H_INCLUDE
